import pandas as pd
from datetime import datetime
import Assassin as a

def main():
    # Initialize error
    error=False

    # Run unit tests
    if not fileNameUnitTests():
        print("Error in unit test: fileNameUnitTests, please see above errors.")
        error=True
    if not eliminatedDateUnitTests():
        print("Error in unit test: eliminatedDateUnitTests, please see above errors.")
        error=True
    if not isAssassinDfUnitTests():
        print("Error in unit test: isAssassinDfUnitTests, please see above errors.")
        error=True
    if not playerRemainingUnitTests():
        print("Error in unit test: playerRemainingUnitTests, please see above errors.")
        error=True
    if not isPlayerEliminatedUnitTests():
       print('Error in unit test: isPlayerEliminatedUnitTests, please see above errors.')
       error=True
    if not isInputDataValidUnitTests():
        print('Error in unit test: isInputDataValidUnitTests, please see above errors.')
        error=True
    if not getMostEliminationsUnitTests():
        print('Error in unit test: getMostEliminationsUnitTests, please see above errors.')
        error=True
    if not getDeadliestDatesUnitTests():
        print('Error in unit test: getDeadliestDatesUnitTests, please see above errors.')
        error=True
    if not initializeEliminatedListUnitTests():
        print('Error in unit test: initializeEliminatedListUnitTests, please see above errors.')
        error=True
    if not updateAssassinDfFromEliminationUnitTests():
        print('Error in unit test: updateAssassinDfFromEliminationUnitTests, please see above errors.')
        error=True
    if not gameOverUnitTests():
        print('Error in unit test: gameOverUnitTests, please see above errors.')
        error=True
    if not assignTargetsUnitTests():
        print('Error in unit test: assignTargetsUnitTests, please see above errors.')
        error=True
    if not playerEliminatedUnitTests():
        print('Error in unit test: playerEliminatedUnitTests, please see above errors.')
        error=True

    # Print either that the unit tests passed or failed.
    if error:
        print('Errors occured during unit tests, please review and double check code modifications and unit tests.')
    else:
        print('All unit tests passed successfully!')

# Decription:
#   Unit tests for the playerEliminated function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def playerEliminatedUnitTests():
    # Initialize error and other tests
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['--', '--', 'Frank', '--','--','Charlie'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', '--'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['--', '--', 'Frank', '--','--','--'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '1/17/2024']
    })
    data3 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data4 = pd.DataFrame({})
    eliminator="Charlie"
    eliminator1="Nancy"
    victim="Frank"
    victim1="Chris"
    victim2="Bob"
    victim3="Alice"
    eliminatedDate="01/21/2024"
    eliminatedDate1="13/21/2024"
    fileName="Assassin<>.xlsx"
    bobsIndex=data[data['Player'] == "Bob"].index
    charliesIndex=data[data['Player'] == "Charlie"].index
    alicesIndex=data[data['Player'] == "Alice"].index
    evesIndex=data[data['Player'] == "Eve"].index
    # Errors
    test=a.playerEliminated(data3,"",eliminator,victim,eliminatedDate)
    test1=a.playerEliminated(data2,"",eliminator,victim,eliminatedDate)
    test2=a.playerEliminated(data,"",eliminator1,victim,eliminatedDate)
    test3=a.playerEliminated(data,"",eliminator,victim1,eliminatedDate)
    test4=a.playerEliminated(data,"",eliminator,victim,eliminatedDate1)
    test5=a.playerEliminated(data,"",eliminator,victim,eliminatedDate,fileName)
    # Actual updates
    test6=a.playerEliminated(data1,"",eliminator,victim,eliminatedDate) # Charlie eliminates frank for the win
    test7=a.playerEliminated(data,"",eliminator,victim2,eliminatedDate) # Charlie eliminates Bob for a normal elimination
    test8=a.playerEliminated(test7,"",eliminator,victim3,eliminatedDate) # Charlie eliminates Alice as a defensive elimination
    test9=a.playerEliminated(data4,"","","","","")

    if not isinstance(test,str) or not isinstance(test1,str) or not isinstance(test2,str) or not isinstance(test3,str) or not isinstance(test4,str) or not isinstance(test5,str) or not isinstance(test9,str):
        print("Error in unit test: playerEliminated did not error out when it should have. Please check code modifications and unit tests.")
        error=True
    if test6!="Charlie":
        print("Error in unit test: playerEliminated did not return the winner when there was one. Please check code modifications and unit tests.")
        error=True
    if not isinstance(test7,pd.DataFrame):
        print("Error in unit test: playerEliminated did not return a data frame when it should have. Please check code modifications and unit tests.")
        error=True
    if test7.iloc[bobsIndex[0],data.columns.get_loc("Assignment")]!="--" or test7.iloc[bobsIndex[0],data.columns.get_loc("Eliminated By")]!="Charlie" or test7.iloc[bobsIndex[0],data.columns.get_loc("Eliminated Date")]!="01/21/2024":
        print("Error in unit test: playerEliminated did not update the DF correctly with Charlie eliminating Bob. Please check code modifications and unit tests.")
        error=True
    if test7.iloc[charliesIndex[0],data.columns.get_loc("Assignment")]!="Diana":
        print("Error in unit test: playerEliminated did not update the DF correctly with Charlie new assignment Diana. Please check code modifications and unit tests.")
        error=True
    if test8.iloc[alicesIndex[0],data.columns.get_loc("Assignment")]!="--" or test8.iloc[alicesIndex[0],data.columns.get_loc("Eliminated By")]!="Charlie" or test8.iloc[alicesIndex[0],data.columns.get_loc("Eliminated Date")]!="01/21/2024":
        print("Error in unit test: playerEliminated did not update the DF correctly with Charlie eliminating Alice. Please check code modifications and unit tests.")
        error=True
    if test8.iloc[evesIndex[0],data.columns.get_loc("Assignment")]!="Charlie":
        print("Error in unit test: playerEliminated did not update the DF correctly with Eve's new assignment for Charlie. Please check code modifications and unit tests.")
        error=True
    
    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the assignTargets function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def assignTargetsUnitTests():
    # Initialize error and other tests
    error=False
    players=['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank']
    players1=['Alice']
    assassinDf=a.assignTargets(players)
    assassinDf1=a.assignTargets(players,"","Assassin<>.xlsx")
    assassinDf2=a.assignTargets(players1)
    assassinDf3=a.assignTargets([],"assassin<>.xlsx","")
    if not isinstance(assassinDf,pd.DataFrame) and not a.isAssassinDf(assassinDf):
        print("Error in unit test: assignTargets did not return a data frame when it should have or did not return a valid assassin data frame. Please review code modifications and unit test.")
        error=True
    if not isinstance(assassinDf1,str) or not isinstance(assassinDf2,str) or not isinstance(assassinDf3,str):
        print("Error in unit test: assignTargets did not error out when it should have. Please review code modifications and unit test.")
        error=True
    assignmentError=False
    for i in range(len(players)):
        if assassinDf.iloc[i,assassinDf.columns.get_loc("Assignment")]==assassinDf.iloc[i,assassinDf.columns.get_loc("Player")]:
            assignmentError=True
    if assignmentError: 
        print("Error in unit test: assignTargets assigned someone themselves, this should not be possible. Please review code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the gameOver function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def gameOverUnitTests():
    # Initialize error and other tests
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['--', '--', 'Frank', '--','--','--'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '1/17/2024']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['--', '--', 'Frank', '--','--','Charlie'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', '--'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '--']
    })
    data3 = pd.DataFrame({})
    gameOverDict={}
    gameOverDict1={}
    test=a.gameOver(data1,"",gameOverDict,"Charlie")
    test1=a.gameOver(data2,"",gameOverDict,"Charlie")
    test2=a.gameOver(data,"",gameOverDict1,"Charlie","gameover<>.xlsx")
    test3=a.gameOver(data3,"",gameOverDict1,"Charlie","")
    gameOver=a.gameOver(data,"",gameOverDict,"Charlie")
    if not isinstance(test,str) or not isinstance(test1,str) or not isinstance(test2,str) or not isinstance(test3,str):
        print("Error in unit test: gameOver did not error out when it should have. Please review code modifications and unit test.")
        error=True
    if not isinstance(gameOver,pd.DataFrame):
        print("Error in unit test: gameOver did not return a dictionary when it should have. Please review code modifications and unit test.")
        error=True
    if gameOver.iloc[0,gameOver.columns.get_loc("Awarded To")]!="Charlie":
        print("Error in unit test: gameOver did not return the correct winner. Please review code modifications and unit test.")
        error=True
    if gameOver.iloc[1,gameOver.columns.get_loc("Awarded To")]!="Charlie with a total of 3 eliminations!":
        print("Error in unit test: gameOver did not return the correct person with most eliminations. Please review code modifications and unit test.")
        error=True
    if gameOver.iloc[2,gameOver.columns.get_loc("Awarded To")]!="1/16/2024 with a total of 2 eliminations!":
        print("Error in unit test: gameOver did not return the correct deadliest date. Please review code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the updateAssassinDfFromElimination function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def updateAssassinDfFromEliminationUnitTests():
    # Initialize error and other tests
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    alicesIndex=data[data['Player'] == "Alice"].index
    charliesIndex=data[data['Player'] == "Charlie"].index
    evesIndex=data[data['Player'] == "Eve"].index
    franksIndex=data[data['Player'] == "Frank"].index
    dianasIndex=data[data['Player'] == "Diana"].index
    if a.updateAssassinDfFromElimination(data1,"Alice","Charlie","01/21/2024")==1:
        print("Error in unit test: updateAssassinDfFromElimination succeeded with invalid input data. Please review code modifications and unit test.")
        error=True
    if a.updateAssassinDfFromElimination(data,"Alice","Charlie","01/21/2024")!=1 or a.updateAssassinDfFromElimination(data,"Eve","Frank","01/21/2024")!=1: # This should update the data with the dates and new assignments for a normal and defensive elimination
        print("Error in unit test: updateAssassinDfFromElimination failed with valid input data. Please review code modifications and unit test.")
        error=True
    if data.iloc[alicesIndex[0],data.columns.get_loc("Assignment")]!="Bob":
        print("Error in unit test: updateAssassinDfFromElimination failed to update Alice's assignment to Bob correctly. Please review code modifications and unit test.")
        error=True
    if data.iloc[charliesIndex[0],data.columns.get_loc("Assignment")]!="--" or data.iloc[franksIndex[0],data.columns.get_loc("Assignment")]!="--":
        print("Error in unit test: updateAssassinDfFromElimination failed to update Charlie's or Frank's assignment to -- correctly. Please review code modifications and unit test.")
        error=True
    if data.iloc[evesIndex[0],data.columns.get_loc("Assignment")]!="Alice":
        print("Error in unit test: updateAssassinDfFromElimination modified Eve's assignment when it shouldnt have because it was a defensive elimination. Please review code modifications and unit test.")
        error=True
    if data.iloc[dianasIndex[0],data.columns.get_loc("Assignment")]!="Eve":
        print("Error in unit test: updateAssassinDfFromElimination failed to update Diana's assignment to Eve correctly from the defensive elimination. Please review code modifications and unit test.")
        error=True
    if data.iloc[charliesIndex[0],data.columns.get_loc("Eliminated By")]!="Alice" or data.iloc[franksIndex[0],data.columns.get_loc("Eliminated By")]!="Eve":
        print("Error in unit test: updateAssassinDfFromElimination failed to update Charlie's or Frank's eliminated by columns to Alice and Eve correctly. Please review code modifications and unit test.")
        error=True
    if data.iloc[charliesIndex[0],data.columns.get_loc("Eliminated Date")]!="01/21/2024" or data.iloc[franksIndex[0],data.columns.get_loc("Eliminated Date")]!="01/21/2024":
        print("Error in unit test: updateAssassinDfFromElimination failed to update Charlie's or Frank's eliminated by columns to Alice and Eve correctly. Please review code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the initializeEliminatedLists function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def initializeEliminatedListUnitTests():
    # Initialize error and other tests
    error=False
    players=['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank']
    numPlayers=len(players)
    numPlayers1=1
    eliminatedBy=[]
    eliminatedDate=[]
    eliminatedBy1=[1,2,3]
    eliminatedDate1=[1,2]
    eliminatedBy2=3
    eliminatedDate2="howdy"
    # These checks should not reach the point where eliminatedBy or eliminatedDate get modified
    if a.initializeEliminatedLists(eliminatedBy,eliminatedDate,numPlayers1)!="ERROR: The number of players entered when trying to initialize the columns for eliminated date and eliminated by was less than 2. You need at least 2 players to play assassin.":
        print("Error in unit test: initializeEliminatedLists returned true or an incorrect error when there was less than 2 players entered. Please double check code modifications and unit test.")
        error=True
    if a.initializeEliminatedLists(eliminatedBy,eliminatedDate1,numPlayers)!="ERROR: The inputs for eliminatedBy and eliminatedDate are not empty lists. Please enter empty lists for those two parameters" or a.initializeEliminatedLists(eliminatedBy1,eliminatedDate,numPlayers)!="ERROR: The inputs for eliminatedBy and eliminatedDate are not empty lists. Please enter empty lists for those two parameters":
        print("Error in unit test: initializeEliminatedLists returned true or an incorrect error when the lists entered were not empty. Please double check code modifications and unit test.")
        error=True
    if a.initializeEliminatedLists(eliminatedBy2,eliminatedDate,numPlayers)!="ERROR: The inputs for eliminatedBy and eliminatedDate are not lists. Please enter empty lists for those two parameters" or a.initializeEliminatedLists(eliminatedBy,eliminatedDate2,numPlayers)!="ERROR: The inputs for eliminatedBy and eliminatedDate are not lists. Please enter empty lists for those two parameters":
        print("Error in unit test: initializeEliminatedLists returned true or an incorrect error when non lists where entered for list parameters. Please double check code modifications and unit test.")
        error=True
    if a.initializeEliminatedLists(eliminatedBy,eliminatedDate,numPlayers)!=1:
        print("Error in unit test: initializeEliminatedLists returned an error when it should not have. Please double check code modifications and unit test.")
        error=True
    if eliminatedBy!=["--","--","--","--","--","--"] or eliminatedDate!=["--","--","--","--","--","--"]:
        print("Error in unit test: initializeEliminatedLists did not setup the lists correctly. Please double check code modifcations and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the getDeadliestDate function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def getDeadliestDatesUnitTests():
    # Initialize error and other tests
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '1/17/2024']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', 'Charlie', 'Eve', 'Bob', '--', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '1/17/2024', '1/16/2024', '--', '1/15/2024']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    data3 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })

    if a.getDeadliestDate(data)!="1/16/2024 with a total of 2 eliminations!":
        print("Error in unit test: getMostEliminations did not return the expected string. Please check code modifications and unit test.")
        error=True
    if a.getDeadliestDate(data1)!="1/15/2024, 1/16/2024, tied with a total of 2 eliminations!":
        print("Error in unit test: getMostEliminations did not return the expected string. Please check code modifications and unit test.")
        error=True
    if a.getDeadliestDate(data2)!="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format.":
        print("Error in unit test: getMostEliminations did not return an error when expected. Please check code modifications and unit test.")
        error=True
    if a.getDeadliestDate(data3)!=0:
        print("Error in unit test: getMostEliminations did not return 0 when expected to. Please check code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the getMostEliminations function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def getMostEliminationsUnitTests():
    # Initialize error and other tests
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', 'Charlie', '--', 'Bob', 'Charlie', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '--', '1/14/2024', '1/16/2024', '1/17/2024']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', 'Charlie', 'Eve', 'Bob', '--', 'Charlie'],
        'Eliminated Date': ['1/15/2024', '1/16/2024', '1/17/2024', '1/14/2024', '--', '1/15/2024']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    data3 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })

    if a.getMostEliminations(data)!="Charlie with a total of 3 eliminations!":
        print("Error in unit test: getMostEliminations did not return the expected string. Please check code modifications and unit test.")
        error=True
    if a.getMostEliminations(data1)!="Eve, Charlie, tied with a total of 2 eliminations each!":
        print("Error in unit test: getMostEliminations did not return the expected string. Please check code modifications and unit test.")
        error=True
    if a.getMostEliminations(data2)!="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format.":
        print("Error in unit test: getMostEliminations did not return an error when expected. Please check code modifications and unit test.")
        error=True
    if a.getMostEliminations(data3)!=0:
        print("Error in unit test: getMostEliminations did not return 0 when expected to. Please check code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the isInputDataValid function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def isInputDataValidUnitTests():
    # Initialize error and examples
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['1/15/2024', '--', '--', '--', '--', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    eliminatedDate="05/04/2024"
    eliminatedDate1="13/40/9999"
    victim="Bob"
    victim1="Alice"
    victim2="Steve"
    victim3=""
    eliminator="Charlie"
    eliminator1="Alice"
    eliminator2=""
    eliminator3="Billy"
    if not isinstance(a.isInputDataValid(data,victim,eliminator,eliminatedDate),int):
        print("Error in unit test: isInputDataValid did not return true for a situation where it should have. Please check code modifications and unit test.")
        error=True
    if not isinstance(a.isInputDataValid(data2,victim,eliminator,eliminatedDate),str) or not isinstance(a.isInputDataValid(data,victim,eliminator,eliminatedDate1),str) or not isinstance(a.isInputDataValid(data,victim1,eliminator,eliminatedDate),str) or not isinstance(a.isInputDataValid(data,victim2,eliminator,eliminatedDate),str):
        print("Error in unit test: isInputDataValid did not return an error when it should have. Please check code modifications and unit test.")
        error=True
    if not isinstance(a.isInputDataValid(data,victim,eliminator1,eliminatedDate),str) or not isinstance(a.isInputDataValid(data,victim,eliminator2,eliminatedDate),str) or not isinstance(a.isInputDataValid(data,victim,eliminator3,eliminatedDate),str) or not isinstance(a.isInputDataValid(data,victim3,eliminator,eliminatedDate),str):
        print("Error in unit test: isInputDataValid did not return an error when it should have. Please check code modifications and unit test.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the isPlayerEliminated function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def isPlayerEliminatedUnitTests():
    # Initlaize error and examples
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    data3 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    if a.isPlayerEliminated(data,"Alice")!=0 or a.isPlayerEliminated(data,"Charlie")!=0 or a.isPlayerEliminated(data,"Frank")!=0:
        print("Error in unit test: isPlayerEliminated returned true on a player that should have been considered in the game. Check code modifications and unit test.")
        error=True
    if a.isPlayerEliminated(data2,'Bob')!=1 or a.isPlayerEliminated(data2,'Frank')!=1 or a.isPlayerEliminated(data2,'Diana')!=1:
        print("Error in unit test: isPlayerEliminated returned false for a player that should have been considered eliminated. Check code modifications and unit test.")
        error=True
    if not isinstance(a.isPlayerEliminated(data3,'Bob'), str) or not isinstance(a.isPlayerEliminated(data2,'Billy John Jones'), str):
        print('Error in unit test: isPlayerEliminated did not return an error when it should have. Check code modifications and unit test.')
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the numPlayersRemaining function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def playerRemainingUnitTests():
    # Initialize error and examples
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['1/15/2024', '--', '--', '--', '--', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', 'Alice', 'Alice', 'Alice', '--', 'Alice'],
        'Eliminated Date': ['--', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    data3 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', 'Charlie', '--', 'Charlie', '--', 'Charlie'],
        'Eliminated Date': ['--', '1/15/2024', '--', '1/15/2024', '--', '1/15/2024']
    })
    data4 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['Eve', 'Charlie', 'Eve', 'Charlie', '--', 'Eve'],
        'Eliminated Date': ['1/15/2024', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    data5 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana': ['Eve', 'Charlie', 'Eve', 'Charlie', '--', 'Eve'],
        'Eliminated Date': ['1/15/2024', '1/15/2024', '1/15/2024', '1/15/2024', '--', '1/15/2024']
    })
    numPlayers=a.numPlayersRemaining(data)
    numPlayers1=a.numPlayersRemaining(data1)
    numPlayers2=a.numPlayersRemaining(data2)
    numPlayers3=a.numPlayersRemaining(data3)
    numPlayers4=a.numPlayersRemaining(data4)
    numPlayers5=a.numPlayersRemaining(data5)
    if numPlayers!=6 or numPlayers1!=5 or numPlayers2!=2 or numPlayers3!=3 or numPlayers4!=1:
        print("Error in unit test: numPlayersRemaining did not return the expected number of players in one of the unit tests. Please check code modifications and unit test.")
        error=True
    if not isinstance(numPlayers5, str):
        print("Error in unit test: numPlayersRemaining did not error out in an instance where it should have. Please check code modifications and unit tests.")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the isAssassinDf function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def isAssassinDfUnitTests():
    # Initialize error and data examples
    error=False
    data = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data1 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Banana Boat': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--']
    })
    data2 = pd.DataFrame({
        'Player': ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve', 'Frank'],
        'Assignment': ['Charlie', 'Diana', 'Bob', 'Frank','Alice','Eve'],
        'Eliminated By': ['--', '--', '--', '--', '--', '--'],
        'Eliminated Date': ['--', '--', '--', '--', '--', '--'],
        'Test': ['1','2','3','4','5','6']
    })
    if not a.isAssassinDf(data):
        print("Error in unit test: isAssassinDf did not return successfully when using a correct Assassin data frame. Please check code modifications and unit tests")
        print(data)
        error=True
    if a.isAssassinDf(data1) or a.isAssassinDf(data2):
        print("Error in unit test: isAssassinDf returned successfully when using an incorrect Assassin data frame. Please check code modifications and unit tests")
        error=True

    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the isValidFileName function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def fileNameUnitTests():
    # Initialize error
    error=False

    # Make sure a valid filename works
    fileName="Assassin.xlsx"
    success=a.isValidFileName(fileName)
    if success != 1:
        print("Error in unit test: isValidFileName did not return successfully when using Assassin.xlsx as the file name.")
        error=True

    # Make sure invalid file names wont work
    fileName1="Assassin/.xlsx"
    success1=a.isValidFileName(fileName1)
    fileName2="Assassin .xlsx"
    success2=a.isValidFileName(fileName2)
    fileName3="Assassin..xlsx"
    success3=a.isValidFileName(fileName3)
    fileName4="Assassin<>.xlsx"
    success4=a.isValidFileName(fileName4)
    fileName5="Assassin"
    success5=a.isValidFileName(fileName5)
    fileName6="Assassin\\.xlsx"
    success6=a.isValidFileName(fileName6)
    fileName7="Ass:assin*?.xlsx"
    success7=a.isValidFileName(fileName7)
    if success1 == 1 or success2 == 1 or success3 == 1 or success4 == 1 or success5 == 1 or success6 == 1 or success7 == 1:
        print("Error in unit test: isValidFileName returned true for an invalid file name.")
        error=True
    
    if error:
        return False
    else:
        return True

# Decription:
#   Unit tests for the isEliminatedDate function from Assassin.py
# Returns:
#   True - the unit tests passed
#   False - the unit tests failed
def eliminatedDateUnitTests():
    # Initialize test strings and error
    error=False
    eliminatedDate='05/04/2024' # should pass
    today=datetime.now()
    eliminatedDate1=today.strftime("%m/%d/%Y")
    eliminatedDate2='13/25/1999' # rest should fail
    eliminatedDate3='04/40/2004'
    eliminatedDate4='6/2/98'
    if not a.isEliminatedDate(eliminatedDate) or not a.isEliminatedDate(eliminatedDate1):
        print("Error in unit test: isEliminatedDate returned false for dates that should have passed, double check code modifications and unit test.")
        error=True

    if a.isEliminatedDate(eliminatedDate2) or a.isEliminatedDate(eliminatedDate3) or a.isEliminatedDate(eliminatedDate4):
        print("Error in unit test: isEliminatedDate returned true for dates that should not have passed, double check code modifications and unit tests.")
        error=True

    if error:
        return False
    else:
        return True

main()