# AssassinPythonProgram
## Name
Assassin Python Program

## Description
This program will take an excel sheet formatted with a single column of names or player identifier and generate a spreadsheet of assignments. You can then use that spreadsheet to keep track of player eliminations as the game progresses. The program comes with a function that will update the excel and reassign players when an elimination occurs and recreate the excel spreadsheets.

## Usage
To use these functions, you should create a new folder that stores the Assassin.py file. You can then use any sort of environment you would like to have the code run, if you want to keep it simple, just open up Visual Studio, open the folder you created, and run the code there. You can keep inputing the excel sheet into the playerEliminated function each time a player is eliminated to update it, or you can expand this to be used in some sort of batch job system if you'd like.

See the InputExample.xlsx and OutputExample.xlsx file for an example of what input might work and an example of what that input will output using the assignTargets function.

## Support
If you have any questions or things to add feel free to leave an issue or email horton_d1@denison.edu.

## Contributing
If you would like to contribute, feel free to create a fork and then a merge request when finished and I'll review it. Please use the same or similar coding practices and documentation for new functions, if I dont know what it does I wont accept the merge. 

## Authors and acknowledgment
Created by: Drake Horton

## License
Not licensed, if you want to use this code for something else, feel free to, its not that complex.

## Project status
Completed!
