import random
import pandas as pd
from datetime import datetime

# Decription:
#   This function takes in a list of players and outputs a data frame where it assigns each value of the list to another randomly as targets. Either a list of players or an input
#   file is required.
# Inputs: 
#   players (req) - a list of players. Ex: ['Alice', 'Bob', 'Charlie', 'Diana', 'Eve']
#   inFileName (req) - the input file name of an xlsx file that contains a single column of player names with a header. See InputExample.xlsx for an example.
#   outFileName (opt) - a filename for the data frame to be exported to for tracking, this should be in form 'filename.xlsx'
# Returns:
#   assignments - A pandas dataframe that has columns Player, Assignment, Eliminated By, and Eliminated Date
#   error - if an error occured
# Excel output:
#   outFileName - if the function runs it will output a excel spreadsheet named fileName
# Unit Test:
#   assignTargetsUnitTests()
def assignTargets(players: list=[],inFileName: str="",outFileName: str=""):
    # Validate Input
    if len(players)==0 and (not inFileName or isValidFileName(inFileName)!=1):
        error="ERROR: The player list provided was empty and the input file name is not a valid file name. Please either enter a list of players or input an excel spreadsheet with a list."
        return error
    
    # If the list of players is entered, use that instead
    lengthPlayers=len(players)
    if lengthPlayers==0:
        df=pd.read_excel(inFileName)
        players=df.iloc[:,0].tolist() # Set players to be the first column in the excel
        lengthPlayers=len(players)
        print(players)
    elif lengthPlayers<2:
        error="ERROR: You need more than one player to play assassin."
        return error

    # Copy and shuffle the list to avoid in-place modifications
    shuffledPlayers = players.copy()
    
    # Shuffle the players
    random.shuffle(shuffledPlayers)
    
    # Assign each player to the next with the last looping back to the first
    assignments = []
    for i in range(lengthPlayers):
        nextPlayer=shuffledPlayers[(i+1)%lengthPlayers]
        assignments.append(nextPlayer)

    # Initialize the eliminatedBy list and eliminatedDate list
    eliminatedBy=[]
    eliminatedDate=[]
    error=initializeEliminatedLists(eliminatedBy,eliminatedDate,lengthPlayers)
    if error!=1:
        return error
    
    # Create Pands data frame 
    assassinDf = pd.DataFrame({
        'Player': shuffledPlayers,
        'Assignment': assignments,
        'Eliminated By': eliminatedBy,
        'Eliminated Date': eliminatedDate,
        })
    
    # Export to excel, check if they want to export with a file name and then validate that it will work
    if outFileName:
        valid=isValidFileName(outFileName)
        if valid==True:
            assassinDf.to_excel(outFileName,index=False)
        else:
            return valid
    
    return assassinDf

# Decription:
#   This function takes in the assignments from the game as well as the eliminator and the victim and updates the assassin data frame with new assignments. 
#   Either assassinDf or inFileName needs to be entered.
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
#   inFileName (req) - an input excel file name that will be updated with an elimination. Should be a assassin data frame structured excel from assignTargets
#   eliminator (req) - the player that got the victim out, this should correspond to however they are stored in the assignment dictionary
#   victim (req) - the player that was eliminated
#   eliminatedDate (req) - the date that the eliminator assassinated the victim, should be in form 'DD/MM/YYYY'
#   fileName (opt) - the file name of the excel output that will be saved
# Returns:
#   backupDf - A modified version of the assassinDf that filled in the eliminated date and eliminated by columns and reassigned targets
#   eliminator - the person who won if they assassinated the last target and are now the champions
#   error - if error occurs and we have not modified assassinDf yet
# Excel output:
#   fileName - if this function is successful it will output an excel of name fileName which will be the modified assassin data frame with the new assignments
#   fileName_backup - if this function is successful it will output a backup excel file named fileName_backup which will be the initial excel entered into the function incase something goes wrong
#   fileName_GameOver - if this function is successful and finds that the eliminator assassinated the last target, it will output the game over excel with the game statstics and winner
# Unit Test:
#   playerEliminatedUnitTests()
def playerEliminated(assassinDf: pd.DataFrame="",inFileName: str="",eliminator: str="",victim: str="",eliminatedDate: str="",fileName: str=""):
    # Transfer input file to data frame
    if assassinDf.empty and (inFileName=="" or isValidFileName(inFileName)!=1):
        error="ERROR: Please enter either an assassin data frame or an input file name."
        return error
    elif assassinDf.empty:
        assassinDf=pd.read_excel(inFileName)

    # Validate the input will work and inialize variables
    champion=False
    valid=isInputDataValid(assassinDf,eliminator,victim,eliminatedDate)
    if valid!=1:
        return valid
    
    # Create a backup copy of the data frame
    backupDf=assassinDf.copy()

    # If theres two players left, the person who got the elimination is the winner! Else, call the function to update the assassinDf
    numPlayers=numPlayersRemaining(backupDf)
    if isinstance(numPlayers,str):
        return numPlayers
    elif numPlayers==1:
        error="ERROR: There is only one player remaining. You should not use this function and use the Game Over function instead."
        return error
    elif numPlayers==2:
        updateAssassinDfFromElimination(backupDf,eliminator,victim,eliminatedDate)
        gameOverDict={}
        gameOverDf=gameOver(backupDf,gameOverDict,eliminator)
        champion=True
    else:
        updateAssassinDfFromElimination(backupDf,eliminator,victim,eliminatedDate)

    # Export to excel, check if they want to export with a file name and then validate that it will work
    if fileName:
        valid=isValidFileName(fileName)
        if valid==True and champion:
            gameOverDf.to_excel(fileName[:-5]+'_GameOver.xlsx',index=False)
            backupDf.to_excel(fileName,index=False)
            assassinDf.to_excel(fileName[:-5]+'_Backup.xlsx',index=False)
        elif valid==True:
            backupDf.to_excel(fileName,index=False)
            assassinDf.to_excel(fileName[:-5]+'_Backup.xlsx',index=False)
        else:
            return valid
        
    # Return based on whether there was a winner or not
    if champion:
        return eliminator
    else:
        return backupDf

# Decription:
#   This function should be run when the game of assassins is complete! This is called by playerEliminated if it determines that a champion has been crowned but can be ran on its own.
#   Either assassinDf or inFileName needs to be entered.
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
#   inFileName (req) - a file name
#   gameOVerDict (req) - an empty dictionary to be filled and made into a data frame
#   winner (req) - the name of the player that won
#   fileName (opt) - the file name of the excel output that will be saved
# Returns:
#   gameOverDf - a data frame of the game over statistics
#   error - if something goes wrong 
# Unit Test:
#   gameOverUnitTests()
def gameOver(assassinDf: pd.DataFrame="",inFileName: str="",gameOverDict: dict={},winner: str="",fileName: str=""):
    # Do input file checks
    if assassinDf.empty and (inFileName=="" or isValidFileName(inFileName)!=1):
        error="ERROR: Please enter either a assassin data frame of an input file."
        return error
    elif assassinDf.empty:
        assassinDf=pd.read_excel(inFileName)
        
    # Validate data frame
    if not isAssassinDf(assassinDf):
        error="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format."
        return error
    if not isinstance(gameOverDict,dict):
        error="ERROR: The gameOverDict passed in is not a dictionary, please pass in a dictionary."
        return error
    if isPlayerEliminated(assassinDf,winner):
        error="ERROR: The winner entered has already been eliminated. Make sure you entered the right name of the winner or update the assassin data frame."
        return error
    if numPlayersRemaining(assassinDf)!=1:
        error="ERROR: There is more than one player remaining, for the game to be over only one player must remain. Please check your assassin data frame."
        return error
    
    # Set the data we need in a dictionary and convert it to a pandas data frame
    gameOverDict={
        'Awards':['Winner','Most Eliminations','Deadliest Date'],
        'Awarded To':[winner,getMostEliminations(assassinDf),getDeadliestDate(assassinDf)]
    }
    gameOverDf=pd.DataFrame(gameOverDict)

    # Export the data to excel if fileName is set
    if fileName:
        valid=isValidFileName(fileName)
        if valid==True:
            gameOverDf.to_excel(fileName,index=False)
        else:
            return valid
    return gameOverDf

# Decription:
#   This function is a helper function to update the assassinDf from an elimination that occured.
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
#   eliminator (req) - the player that got the victim out, this should correspond to however they are stored in the assignment dictionary
#   victim (req) - the player that was eliminated
#   eliminatedDate (req) - the date that the eliminator assassinated the victim, should be in form 'MM/DD/YYYY'
# Returns:
#   1 - if successful
#   error - error message if not successful
# Unit Test:
#   updateAssassinDfFromEliminationUnitTests()
def updateAssassinDfFromElimination(assassinDf: pd.DataFrame,eliminator: str,victim: str,eliminatedDate: str):
    # Double check input data incase we didnt before the function was called
    valid=isInputDataValid(assassinDf,eliminator,victim,eliminatedDate)
    if valid!=1:
        return valid
    
    # Get necessary information
    victimPlayerIndex=assassinDf[assassinDf['Player'] == victim].index
    victimAssignmentIndex=assassinDf[assassinDf['Assignment'] == victim].index
    eliminatorPlayerIndex=assassinDf[assassinDf['Player'] == eliminator].index
    
    # Figure out if this was a defensive elimination or not, will also use this if its the final 2
    targetElim = (assassinDf.iloc[eliminatorPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]==victim)

   # If it was a target elimination, reassign the person who was assigned the victim to the eliminator, if not, reassign the eliminator to the victims target
    if targetElim:
        if assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]==eliminator:
            assassinDf.iloc[eliminatorPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]="--" # If we get here and the victims target is the eliminator, theres only two players left
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]="--" # Update the victims assignment to be '--' since they are out of the game
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated By')]=eliminator # Update the victims eliminator
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated Date')]=eliminatedDate # Update the victims eliminated date
        else:
            assassinDf.iloc[eliminatorPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]=assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Assignment')] # Update the eliminators assignment to whoever the victim had
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]="--" # Update the victims assignment to be '--' since they are out of the game
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated By')]=eliminator # Update the victims eliminator
            assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated Date')]=eliminatedDate # Update the victims eliminated date
    else:
        victimsHunter=assassinDf.iloc[victimAssignmentIndex[0],assassinDf.columns.get_loc('Player')] # Get the victims hunter since it was a defensive elim
        victimHunterPlayerIndex=assassinDf[assassinDf['Player'] == victimsHunter].index # Get that players index in the row of the data frame
        assassinDf.iloc[victimHunterPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]=eliminator # Update the victims hunter's assignment to the eliminator
        assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Assignment')]="--" # Update the victims assignment to be '--' since they are out of the game
        assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated By')]=eliminator # Update the victims eliminator
        assassinDf.iloc[victimPlayerIndex[0],assassinDf.columns.get_loc('Eliminated Date')]=eliminatedDate # Update the victims eliminated date
    
    return 1

# Decription:
#   This function is a helper function for use in assignTargets, this initializes the eliminated by and eliminated date columns for the pandas data structure
# Inputs: 
#   eliminatedBy (req) - an empty list
#   eliminatedDate (req) - an empty list
#   numPlayers (req) - the number of players in the game of assassin
# Returns:
#   1 - if successful
#   error - if an error occured
# Unit Test:
#   initializeEliminatedListUnitTests()
def initializeEliminatedLists(eliminatedBy: list,eliminatedDate: list,numPlayers: int):
    # Validate input
    if numPlayers<2:
        error="ERROR: The number of players entered when trying to initialize the columns for eliminated date and eliminated by was less than 2. You need at least 2 players to play assassin."
        return error
    if not isinstance(eliminatedBy,list) or not isinstance(eliminatedDate,list):
        error="ERROR: The inputs for eliminatedBy and eliminatedDate are not lists. Please enter empty lists for those two parameters"
        return error
    if len(eliminatedBy)>0 or len(eliminatedDate)>0:
        error="ERROR: The inputs for eliminatedBy and eliminatedDate are not empty lists. Please enter empty lists for those two parameters"
        return error

    # Set up the lists for the data frame
    for i in range(numPlayers):
        eliminatedBy.append("--")
        eliminatedDate.append("--")
    if len(eliminatedBy)==numPlayers:
        return 1
    else:
        error="ERROR: An error occured in the initializeEliminatedLists() function, the number of assigned players did not match what was stored in the inital eliminated lists."
        return error

# Decription:
#   This function is a helper function for excel outputs, this checks to make sure the fileName is a valid file name
# Inputs: 
#   fileName (req) - the file name to save the xlsx file to, should be in form 'filename.xlsx'
# Returns:
#   True - if successful
#   error - an error string if the fileName was not valid
# Unit Test:
#   fileNameUnitTests()
def isValidFileName(fileName: str):
    # Check to make sure it is a string
    if not isinstance(fileName,str):
        error="ERROR: The file name entered in assignTargets is not a string, it should be a string. File name entered: "+fileName
        return error
    
    # Check to see if it ends with xlsx
    if not fileName.endswith('.xlsx'):
        error="ERROR: The file name entered does not end with .xlsx, the filename should be in form filename.xlsx. File name entered: "+fileName
        return error
    
    # Check for invalid characters
    invalidChar='<>:"/\\|?*'
    if any(char in invalidChar for char in fileName):
        error="ERROR: The file name entered contains invalid characters, please remove them from the file name. File name entered: "+fileName
        return error

    # Check for trailing space or period
    if fileName[-6] in ' .':
        error="ERROR: The entered file name ends with a space or a period, please modify the file name to remove them. File name entered: "+fileName
        return error

    return True

# Decription:
#   This function is a helper function to determine how many players have not been eliminatedd yet in the assassin game
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
# Returns:
#   aliveCount - number of players in the game that have not been eliminated
#   error - an error string if the fileName was not valid
# Unit Test:
#   playerRemainingUnitTests()
def numPlayersRemaining(assassinDf: pd.DataFrame):
    # Double check assassinDf passed in
    if not isAssassinDf(assassinDf):
        error="ERROR: The assassinDf passed into numPLayersRemaining is not in the proper format. Please format the data frame correctly."
        return error
    # Get the count of "--" from the Eliminated By column
    aliveCount=assassinDf['Eliminated By'].value_counts().get('--',0)
    if aliveCount<1:
        error="ERROR: There is less than one player alive. This should not be possible. Please double check your assassin data frame."
        return error
    return aliveCount

# Decription:
#   This function is a helper function for use in playerEliminated() and before calling the updateAssassinDfFromElimination() function, this will check to make sure the victim 
#   and eliminator names entered into the function are actually in the assassin data frame before making any modifications to it. It makes sure that the eliminator and victim 
#   are both in the game and someone was assigned to eliminated them.
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
#   eliminator (req) - the name of the eliminator
#   victim (req) - the name of the victim
#   eliminatedDate (req) - a date in format MM/DD/YYYY
# Returns:
#   1 - if successful
#   error - an error string if the input data was not valid
# Unit Test:
#   isInputDataValidUnitTests()
def isInputDataValid(assassinDf: pd.DataFrame,eliminator: str,victim: str,eliminatedDate: str):
    # Check assassinDf to make sure its the right format
    if not isAssassinDf(assassinDf):
        error="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format."
        return error
    
    # Check eliminateDate to make sure its in the right format
    if not isEliminatedDate(eliminatedDate):
        error="ERROR: The eliminated date entered is not in a valid MM/DD/YYYY format. Please edit the eliminated date format."
        return error

    # Get necessary data to make sure the victim and the eliminator had a row and was assigned to someone
    victimPIndex=assassinDf[assassinDf['Player'] == victim].index
    victimAIndex=assassinDf[assassinDf['Assignment'] == victim].index
    eliminatorPIndex=assassinDf[assassinDf['Player'] == eliminator].index
    eliminatorAIndex=assassinDf[assassinDf['Assignment'] == eliminator].index

    # Check to see if victim and the eliminator had a row and was assigned to someone
    if len(victimPIndex) == 0:
        error="ERROR: The victim entered ("+victim+") does not have a index in the 'Player' column in the entered assassin data frame. Please double check the victimn exists in that column and spelling."
        return error
    elif len(victimAIndex) == 0:
        error="ERROR: The victim entered ("+victim+") does not have a index in the 'Assignment' column in the entered assassin data frame. Please double check the victimn exists in that column and spelling."
        return error
    elif len(eliminatorPIndex) == 0:
        error="ERROR: The eliminator entered ("+eliminator+") does not have a index in the 'Player' column in the entered assassin data frame. Please double check the eliminator exists in that column and spelling."
        return error
    elif len(eliminatorAIndex) == 0:
        error="ERROR: The eliminator entered ("+eliminator+") does not have a index in the 'Assignment' column in the entered assassin data frame. Please double check the eliminator exists in that column and spelling."
        return error
    elif isPlayerEliminated(assassinDf,victim):
        error="ERROR: The victim entered is already dead ("+victim+"). Please double check you've enetered the right name."
        return error
    elif isPlayerEliminated(assassinDf,eliminator):
        error="ERROR: The eliminator entered is already dead ("+eliminator+"). Please double check you've enetered the right name."
        return error
    else:
        return 1

# Decription:
#   This function is a helper function to determine if a data frame is indeed an assassin data frame
# Inputs: 
#   assassinDf (req) - a pandas data structure created from assignTargets()
# Returns:
#   True - the data frame entered is in form of an assassin data frame
#   False - the data frame is not in the proper format 
# Unit Test:
#   isAssassinDfUnitTests()  
def isAssassinDf(assassinDf: pd.DataFrame):
    # Setup checks
    reqCols=['Player','Assignment','Eliminated By','Eliminated Date']
    expectedNumCols=len(reqCols)

    # Check to make sure we only have the expected 4 columns
    if len(assassinDf.columns)!=expectedNumCols:
        return False

    # Check to make sure the data frame columns are indeed the ones we want
    for column in reqCols:
        if column not in assassinDf.columns:
            return False

    return True

# Decription:
#   This function is a helper function to determine if an elimination date is indeed a date in the right format
# Inputs: 
#   eliminatedDate (req) - a string date in form MM/DD/YYYY
# Returns:
#   True - the date is formatted correctly
#   False - the date is not formatted correctly
# Unit Test:
#   eliminatedDateUnitTests()
def isEliminatedDate(eliminatedDate: str):
    # Try to parse the eliminated date string in MM/DD/YYYY format
    try:
        datetime.strptime(eliminatedDate,'%m/%d/%Y')
        return True
    except ValueError:
        return False

# Decription:
#   This function is a helper function to determine if a player is eliminated already or not
# Inputs:
#   assassinDf (req) - a pandas data structure created from assignTargets()
#   player (req) - a player in the assassin game
# Returns:
#   1 - the player entered is eliminated
#   0 - the player is not eliminated
#   error - input error
# Unit Test:
#   isPlayerEliminatedUnitTests()
def isPlayerEliminated(assassinDf: pd.DataFrame,player: str):
    # Validate input
    if not isAssassinDf(assassinDf):
        error="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format."
        return error
    
    # Find the index of the player passed in
    playerIndex=assassinDf[assassinDf['Player'] == player].index
    if playerIndex.empty:
        error="ERROR: The player enetered is not in the assassin data frame entered into the tag."
        return error
    
    # Check to see if their eliminated by column is still blank
    if assassinDf.iloc[playerIndex[0],assassinDf.columns.get_loc('Eliminated By')]=="--":
        return 0
    else:
        return 1

# Decription:
#   This function is a helper function to calculate which player(s) got or currently have the most elminiations
# Inputs:
#   assassinDf (req) - a pandas data structure created from assignTargets()
# Returns:
#   mostEliminations - A string detailing which player(s) have the most eliminations
#   0 - if no one has gotten any eliminations yet
#   error - input error
# Unit Test:
#   getMostEliminationsUnitTests()
def getMostEliminations(assassinDf: pd.DataFrame):
    # Validate input
    if not isAssassinDf(assassinDf):
        error="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format."
        return error
    
    # Filter out '--'
    filterAssassinDf=assassinDf[assassinDf['Eliminated By']!='--']

    # Check to see if it's empty
    if filterAssassinDf.empty:
        return 0
    
    # Count the occurrences of each player in the column
    eliminationCnt=filterAssassinDf['Eliminated By'].value_counts()
    
    # Find the max number of eliminations
    maxElim=eliminationCnt.max()

    # Find all players with the maximum number of eliminations
    topEliminators=eliminationCnt[eliminationCnt==maxElim].index.tolist()

    # Setup return string
    if len(topEliminators)==1:
        mostEliminations=topEliminators[0]+" with a total of "+str(maxElim)+" eliminations!"
    else:
        topEliminatorsStr=', '.join(str(item) for item in topEliminators)
        mostEliminations=topEliminatorsStr+", tied with a total of "+str(maxElim)+" eliminations each!"

    return mostEliminations

# Decription:
#   This function is a helper function to calculate which date is the deadliest date(s) so far
# Inputs:
#   assassinDf (req) - a pandas data structure created from assignTargets()
# Returns:
#   deadlistDate - A string detailing which date(s) have the most eliminations on them
#   0 - if no one has gotten any eliminations yet
#   error - input error
# Unit Test:
#   getDeadliestDatesUnitTests()
def getDeadliestDate(assassinDf: pd.DataFrame):
    # Validate input
    if not isAssassinDf(assassinDf):
        error="ERROR: The data frame entered is not in the form a assassin data frame should be in. Please check to make sure your excel sheet is in the right format."
        return error
    
    # Filter out '--'
    filterAssassinDf=assassinDf[assassinDf['Eliminated Date']!='--']

    # Check to see if it's empty
    if filterAssassinDf.empty:
        return 0
    
    # Count the occurrences of each player in the column
    dateElimCnt=filterAssassinDf['Eliminated Date'].value_counts()

    # Find the max number of eliminations
    mostElimCnt=dateElimCnt.max()

    # Find all players with the maximum number of eliminations
    dates=dateElimCnt[dateElimCnt==mostElimCnt].index.tolist()

    # Setup return string
    if len(dates)==1:
        deadliestDates=dates[0]+" with a total of "+str(mostElimCnt)+" eliminations!"
    else:
        deadliestDatesStr=', '.join(str(item) for item in dates)
        deadliestDates=deadliestDatesStr+", tied with a total of "+str(mostElimCnt)+" eliminations!"

    return deadliestDates